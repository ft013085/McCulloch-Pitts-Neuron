#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib> // For system()

using namespace std;

class McCullochPittsNeuron {
private:
    std::vector<double> inputs;
    std::vector<double> weights;
    double threshold;
public:
    McCullochPittsNeuron(int num_inputs, double thres) : threshold(thres) {
        // Initialize weights with random values between -1 and 1
        for (int i = 0; i < num_inputs; ++i) {
            weights.push_back(((double)rand() / RAND_MAX) * 2 - 1);
        }
    }

    // Set inputs
    void setInputs(const std::vector<double>& in) {
        inputs = in;
    }

    // Set weights
    void setWeights(const std::vector<double>& w) {
        weights = w;
    }

    // Set threshold
    void setThreshold(double thres) {
        threshold = thres;
    }

    // Perform linear separation
    double calculateOutput() {
        double sum = 0;
        for (int i = 0; i < inputs.size(); ++i) {
            sum += inputs[i] * weights[i];
        }
        return sum;
    }
};

void writeDataToFile(const std::vector<double>& input_values, const std::vector<double>& weight_values, double threshold, double output) {
    // Output data to a file
    ofstream outfile("data.txt");
    if (outfile.is_open()) {
        for (auto val : input_values) {
            outfile << val << " ";
        }
        outfile << endl;

        for (auto val : weight_values) {
            outfile << val << " ";
        }
        outfile << endl;

        outfile << threshold << endl;

        outfile << output << endl;

        outfile.close();
    }
    else {
        cout << "Unable to open file for writing.";
        exit(1);
    }
}

// Function to write data to file in table format
void writeTableToFile(const std::vector<double>& x1_values, const std::vector<double>& x2_values, const std::vector<int>& target_values) {
    // Output data to a file
    ofstream outfile("table.txt");
    if (outfile.is_open()) {
        // Write x1 values
        for (auto val : x1_values) {
            outfile << val << " ";
        }
        outfile << endl;

        // Write x2 values
        for (auto val : x2_values) {
            outfile << val << " ";
        }
        outfile << endl;

        // Write target values
        for (auto val : target_values) {
            outfile << val << " ";
        }
        outfile << endl;

        outfile.close();
    }
    else {
        cout << "Unable to open file for writing.";
        exit(1);
    }
}

int main() {
    cout << ">> Running main.cpp\n" << endl;
    // Example usage
    int num_inputs = 3;
    double threshold = 0; // Threshold not used for linear separation

    McCullochPittsNeuron neuron(num_inputs, threshold);

    std::vector<double> input_values = { 1, -4, 1 };
    neuron.setInputs(input_values);

    std::vector<double> weight_values = { 1, 2, 3 };
    neuron.setWeights(weight_values);

    double output = neuron.calculateOutput();

    // Output data for plotting
    cout << "Input Values: ";
    for (auto val : input_values) {
        cout << val << " ";
    }
    cout << endl;

    cout << "Weight Values: ";
    for (auto val : weight_values) {
        cout << val << " ";
    }
    cout << endl;

    cout << "Threshold: " << threshold << endl;

    cout << "Output: " << output << endl;

    // Write data to file
    writeDataToFile(input_values, weight_values, threshold, output);

    std::vector<double> x1_values = { 2, 3, 2, -1, 4, 1 };
    std::vector<double> x2_values = { 3, -1, -3, 2, 4, -4 };
    std::vector<int> target_values = { 1, 0, 0, 1, 0, 1 };

    // Write data to file
    writeTableToFile(x1_values, x2_values, target_values);

    // Execute Python script
    system("python plot_data.py");

    return 0;
}
