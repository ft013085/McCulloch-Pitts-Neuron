import numpy as np
import matplotlib.pyplot as plt

def read_data(file_name):
    # Read data from the given file
    with open(file_name, "r") as file:
        lines = file.readlines()
        x1_values = np.array(list(map(float, lines[0].split())))  
        x2_values = np.array(list(map(float, lines[1].split())))  
        target_values = np.array(list(map(int, lines[2].split())))  
    return x1_values, x2_values, target_values

def detect_outliers(x1_values, x2_values, threshold):
    # Combine x1 and x2 values into a single array
    data = np.concatenate((x1_values, x2_values))
    
    # Calculate the median absolute deviation (MAD)
    mad = np.median(np.abs(data - np.median(data)))
    
    # Use the MAD to determine the outlier threshold
    outlier_threshold = threshold * mad
    
    # Detect outliers based on the calculated threshold
    outliers = np.concatenate((x1_values[np.abs(x1_values - np.median(data)) > outlier_threshold],
                               x2_values[np.abs(x2_values - np.median(data)) > outlier_threshold]))
    
    return outliers, outlier_threshold

def update_weights(w0, w1, w2, x1, x2, target, learning_rate):
    # Update weights using the delta rule
    output = w0 + w1 * x1 + w2 * x2
    delta_w0 = learning_rate * (target - output) * 1  # x0 = 1
    delta_w1 = learning_rate * (target - output) * x1
    delta_w2 = learning_rate * (target - output) * x2
    return w0 + delta_w0, w1 + delta_w1, w2 + delta_w2

def calculate_range(data):
    # Calculate the range and margin for the given data
    data_range = max(data) - min(data)
    margin = 0.1 * max(abs(min(data)), abs(max(data)))
    lower_limit = min(data) - margin
    upper_limit = max(data) + margin
    return lower_limit, upper_limit

def plot_data(x1_values, x2_values, target_values, w0, w1, w2):
    # Calculate the range and margins for x and y values
    x_lower, x_upper = calculate_range(x1_values)
    y_lower, y_upper = calculate_range(x2_values)
    
    # Plot the given data and separation line
    plt.figure(figsize=(8, 6))
    colors = ['blue' if target == 1 else 'red' for target in target_values]
    plt.scatter(x1_values, x2_values, c=colors, marker='o')
    if w2 != 0:
        m = -w1 / w2
        c = (0.5 - w0) / w2
        
        # Generate x values for the separation line
        x_values = np.linspace(x_lower, x_upper, 100)
        
        # Calculate y values for the separation line
        y_values = m * x_values + c
        
        # Plot the separation line
        plt.plot(x_values, y_values, color='green', linestyle='--', label='Separation Line')
        
        # Print the equation of the separation line
        print("The equation of the seperation line in form y= mx+c")
        print("In terms of the neuron: x2 = -(w1/w2)x1 + (0.5 - w0) / w2")
        print(f"With w1 = {w1:.2f}, w2 = {w2:.2f}, and w0 = {w0:.2f}:")
        print(f"x2 = {m:.2f}x1 + {c:.2f}")

    
    plt.axhline(0, color='black', linestyle='--', linewidth=0.5)
    plt.axvline(0, color='black', linestyle='--', linewidth=0.5)
    plt.xlim(x_lower, x_upper)
    plt.ylim(y_lower, y_upper)
    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.title('Scatter Plot of x1 vs x2 with Target Labels and Separation Line')
    plt.grid(True)
    plt.tight_layout()
    plt.show()


def check_linear_separability(x1_values, x2_values, target_values, w0, w1, w2):
    # Check if the data is linearly separable
    for x1, x2, target in zip(x1_values, x2_values, target_values):
        output = w0 + w1 * x1 + w2 * x2
        if (output < 0.5 and target == 1) or (output >= 0.5 and target == 0):
            return False
    return True

def main():
    # Main function
    print("\n>> Running plot_data.py")
    file_name = "table.txt"
    x1_values, x2_values, target_values = read_data(file_name)
    
    # Detect outliers
    outlier_threshold = 10  # Adjust this threshold as needed
    outliers, threshold = detect_outliers(x1_values, x2_values, outlier_threshold)
    
    if len(outliers) > 0:
        print("Outliers detected:")
        print(outliers)
    
    # Remove outliers from the data
    x1_values_filtered = np.array([x1 for x1, x2 in zip(x1_values, x2_values) if (x1 not in outliers and x2 not in outliers)])
    x2_values_filtered = np.array([x2 for x1, x2 in zip(x1_values, x2_values) if (x1 not in outliers and x2 not in outliers)])
    target_values_filtered = np.array([target for (x1, x2), target in zip(zip(x1_values, x2_values), target_values) if (x1 not in outliers and x2 not in outliers)])
    
    w0, w1, w2 = 1000.91, 504.4, -693  # Initial values for w0, w1, w2
    learning_rate = 0.1
    max_iterations = 10000
    
    # Iterate until convergence or maximum iterations reached
    for _ in range(max_iterations):
        converged = True
        for x1, x2, target in zip(x1_values_filtered, x2_values_filtered, target_values_filtered):
            output = w0 + w1 * x1 + w2 * x2
            if (output < 0.5 and target == 1) or (output >= 0.5 and target == 0):
                w0, w1, w2 = update_weights(w0, w1, w2, x1, x2, target, learning_rate)
                converged = False
        if converged:
            break
    
    if converged:
        print("Converged after {} iterations".format(_ + 1))
        if check_linear_separability(x1_values_filtered, x2_values_filtered, target_values_filtered, w0, w1, w2):
            print("Data is linearly separable")
            plot_data(x1_values_filtered, x2_values_filtered, target_values_filtered, w0, w1, w2)
        else:
            print("Data is not linearly separable")
    else:
        print("Maximum iterations reached. Convergence not achieved.")

if __name__ == "__main__":
    main()
